## shadowsocks-rust

[shadowsocks rustlang port](https://github.com/shadowsocks/shadowsocks-rust)
packaging, which products the latest version in `.deb` format. Since this is
the latest version, you are running on the cutting edge if you choose to
install.

## Usage (APT installation)

1. Set up the APT key

    Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/shadowsocks-rust/pubkey.gpg

    or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/shadowsocks-rust/pubkey.asc

2. Set up APT sources

    Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list`
    with the content as following (debian/bookworm for example):

        deb https://xdeb.gitlab.io/shadowsocks-rust/debian bookworm contrib

    The available source list files are:

    * Debian 11 (bullseye)

        deb https://xdeb.gitlab.io/shadowsocks-rust/debian bullseye contrib

    * Debian 12 (bookworm)

        deb https://xdeb.gitlab.io/shadowsocks-rust/debian bookworm contrib

    * Ubuntu 20.04 TLS (Focal Fossa)

        deb https://xdeb.gitlab.io/shadowsocks-rust/ubuntu focal universe

    * Ubuntu 22.04 TLS (Jammy Jellyfish)

        deb https://xdeb.gitlab.io/shadowsocks-rust/ubuntu jammy universe

    * Ubuntu 24.04 LTS (Noble Numbat)

        deb https://xdeb.gitlab.io/shadowsocks-rust/ubuntu noble universe

3. Install the package

       apt-get update && apt-get install shadowsocks-rust

4. Suggested packages (optional)

    It's highly recommended to install `v2ray-plugin` and `haveged` packages.

    * `haveged` helps low-end box to run `shadowsocks-rust` faster since "is a
      userspace entropy daemon which is not dependent upon the standard
      mechanisms for harvesting randomness for the system entropy pool".

    * `v2ray-plugin` is a shadowsocks SIP3 plugin which is often used.

## Usage (binaries downloading & install)

All binaries are stored at `https://xdeb.gitlab.io/shadowsocks-rust/shadowsocks-rust_<OS>_<ARCH>.tar.gz`.

For example, you can download x64 binary for _Debian 12_ at
<https://xdeb.gitlab.io/shadowsocks-rust/shadowsocks-rust_bookworm_amd64.tar.gz>. You can also
download i386 binary for _Ubuntu 24.04_ at
<https://xdeb.gitlab.io/shadowsocks-rust/shadowsocks-rust_noble_i386.tar.gz>. Then unzip the
downloaded file by `tar -xz` command.

For instance, download the binary on _Debian 12 (amd64)_, and install it, we
can use a one-line command like,

    wget -O- https://xdeb.gitlab.io/shadowsocks-rust/shadowsocks-rust_bookworm_amd64.tar.gz | tar -xzC /usr/local/bin/

Then the 5 binaries (`sslocal`, `ssmanager`, `ssserver`, `ssservice` and `ssurl`) will be installed in `/usr/local/bin` path.

## Usage (docker)

Note: All docker images are built with v2ray-plugin (https://gitlab.com/xdeb/v2ray-plugin).

    docker pull registry.gitlab.com/xdeb/shadowsocks-rust[:<version>]

    docker pull registry.gitlab.com/xdeb/shadowsocks-rust:[<version>-]local

    docker pull registry.gitlab.com/xdeb/shadowsocks-rust:[<version>-]server

    docker pull registry.gitlab.com/xdeb/shadowsocks-rust:[<version>-]alpine

    docker pull registry.gitlab.com/xdeb/shadowsocks-rust:[<version>-]local-alpine

    docker pull registry.gitlab.com/xdeb/shadowsocks-rust:[<version>-]server-alpine
