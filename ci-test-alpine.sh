#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\[\033[1;93m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'
else
    PS4='\[\033[1;93m\]$(date +%FT%T%z) ${0}:${LINENO}\n+ \e[m\]'
fi

. /etc/os-release

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

if [ "$ID" != alpine"" ]; then
    echo "ERROR: ID not found: $ID"
    exit 1
fi


################
##            ##
##  DO TESTS  ##
##            ##
################

./dist/x86_64/alpine/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssurl --version     | grep "^ssurl $PKG_VERSION$"

./dist/i686/alpine/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssurl --version     | grep "^ssurl $PKG_VERSION$"


exit 0
