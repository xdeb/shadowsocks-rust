#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\[\033[1;94m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'
else
    PS4='\[\033[1;94m\]$(date +%FT%T%z) ${0}:${LINENO}\n+ \e[m\]'
fi

. /etc/os-release

function cargo-cache() { sh -c "if command -v cargo-cache >/dev/null; then command cargo-cache $* ; fi" ; }
function sccache() { sh -c "if command -v sccache >/dev/null; then command sccache $* ; fi" ; }

set -eux


###########################
##                       ##
##  SET UP BUILDING ENV  ##
##                       ##
###########################

case $ID in
debian)
    case $VERSION_CODENAME in
    bullseye|bookworm)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
esac
export CODENAME

# Prepare `file` command
if ! command -v file >/dev/null; then
    case $CODENAME in
    bullseye|bookworm)
        apt-get update
        apt-get install --no-install-recommends --yes file
        ;;
    *)
        echo "ERROR: CODENAME not found: $CODENAME"
        exit 1
    esac
fi

case $TARGET_ARCH in
x86_64)
    target=x86_64-unknown-linux-gnu
    ;;
i686)
    target=i686-unknown-linux-gnu

    rustup target add i686-unknown-linux-gnu

    # install cross compile dependencies
    apt-get update
    apt-get install --yes gcc-multilib
    ;;
*)
    echo "ERROR: TARGET_ARCH not found: $TARGET_ARCH"
    exit 1
esac

src_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
dist_dir=$CI_PROJECT_DIR/dist
target_dir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION/target/$target

# Restore the building cache
mkdir -p .cache
cache_hash_0=""
if [ -f .cache/${PKG_NAME}-${PKG_VERSION}-$CODENAME-$TARGET_ARCH.tar.gz ]; then
    echo "** restore cache"

    mkdir -p $src_dir/target
    tar x -C $src_dir/target -f .cache/${PKG_NAME}-${PKG_VERSION}-$CODENAME-$TARGET_ARCH.tar.gz

    cache_hash_0=$(find $src_dir -type f -exec md5sum {} \; | md5sum)
fi

cd $src_dir


################
##            ##
##  DO BUILD  ##
##            ##
################

# Build binaries
cargo build --release --features="local-redir,local-dns,local-tun" --target=$target
cargo-cache local
sccache --show-stats

install -Dv -t $dist_dir/$TARGET_ARCH/$CODENAME/ $target_dir/release/{sslocal,ssmanager,ssserver,ssservice,ssurl}
strip --strip-all --verbose $dist_dir/$TARGET_ARCH/$CODENAME/{sslocal,ssmanager,ssserver,ssservice,ssurl}

file $dist_dir/$TARGET_ARCH/$CODENAME/{sslocal,ssmanager,ssserver,ssservice,ssurl}
ldd $dist_dir/$TARGET_ARCH/$CODENAME/{sslocal,ssmanager,ssserver,ssservice,ssurl}

$dist_dir/$TARGET_ARCH/$CODENAME/sslocal --version
$dist_dir/$TARGET_ARCH/$CODENAME/ssmanager --version
$dist_dir/$TARGET_ARCH/$CODENAME/ssserver --version
$dist_dir/$TARGET_ARCH/$CODENAME/ssservice --version
$dist_dir/$TARGET_ARCH/$CODENAME/ssurl --version

# Build packages
if [   "${PKG_VERSION}" == "1.8.23" \
    -o "${PKG_VERSION}" == "1.9.0"  \
    -o "${PKG_VERSION}" == "1.9.1"  \
    -o "${PKG_VERSION}" == "1.10.9" \
    -o "${PKG_VERSION}" == "1.11.0" \
    -o "${PKG_VERSION}" == "1.12.5" \
    -o "${PKG_VERSION}" == "1.13.1" \
] ; then
    cp -v ../src/changelog debian/changelog ;
fi

mkdir -p $dist_dir
cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~$CODENAME
cargo-cache local
sccache --show-stats

case $CODENAME in
bullseye)
    cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~focal
    ;;
bookworm)
    cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~jammy
    cargo deb --no-build --strip --target=$target --output=$dist_dir --deb-version=$PKG_VERSION-$PKG_RELEASE-xdeb~noble
    ;;
esac

# Check outputs
ls -lAF --color $dist_dir/ $dist_dir/*/*/


###############
##           ##
##  DO TEST  ##
##           ##
###############

# FIXME: See issue #2 - CI fails when enable local-dns
#
# The test should be:
#
#     `cargo test --release --features="local-redir,local-dns,local-tun"`
#
# But enabling local-dns causes CI hang at test step.
#
# It passes if test in container directly on the self-hosted GitLab Runner
# server. Suspect it's due to runner's config.
#
# Temporary workaround - disable local-dns for now.
#
# @maintainer: Re-enable after investigating runner container config.
cargo test --release --features="local-redir,local-tun" --target=$target
cargo-cache local
sccache --show-stats


##############
##          ##
##  FINISH  ##
##          ##
##############

# Create the building cache
cache_hash_1=$(find $src_dir -type f -exec md5sum {} \; | md5sum)
if [ "$cache_hash_0" != "$cache_hash_1" ]; then
    echo "** update cache"
    tar c -C $src_dir/target . | gzip -1 > $CI_PROJECT_DIR/.cache/$PKG_NAME-$PKG_VERSION-$CODENAME-$TARGET_ARCH.tar.gz
else
    echo "** cache not changed, skip to create new one"
fi


exit 0
