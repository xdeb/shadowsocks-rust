#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\[\033[1;93m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'
else
    PS4='\[\033[1;93m\]$(date +%FT%T%z) ${0}:${LINENO}\n+ \e[m\]'
fi

. /etc/os-release

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

case $ID in
debian|ubuntu)
    case $VERSION_CODENAME in
    bullseye|bookworm|focal|jammy|noble)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac


################
##            ##
##  DO TESTS  ##
##            ##
################

# TEST: binary (static linked)
./dist/x86_64/alpine/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
./dist/x86_64/alpine/ssurl --version     | grep "^ssurl $PKG_VERSION$"

./dist/i686/alpine/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
./dist/i686/alpine/ssurl --version     | grep "^ssurl $PKG_VERSION$"

# TEST: binary (dynamic linked)
case $CODENAME in
bullseye|focal)
    ./dist/x86_64/bullseye/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssurl --version     | grep "^ssurl $PKG_VERSION$"
    ;;
bookworm|jammy|noble)
    ./dist/x86_64/bullseye/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bullseye/ssurl --version     | grep "^ssurl $PKG_VERSION$"

    ./dist/x86_64/bookworm/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bookworm/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bookworm/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bookworm/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
    ./dist/x86_64/bookworm/ssurl --version     | grep "^ssurl $PKG_VERSION$"
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac

apt-get update

# TEST: install .deb package
apt-get install -y ./dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssurl --version     | grep "^ssurl $PKG_VERSION$"

# TEST: uninstall .deb package
test -x /usr/bin/sslocal
test -x /usr/bin/ssmanager
test -x /usr/bin/ssserver
test -x /usr/bin/ssservice
test -x /usr/bin/ssurl
apt-get autoremove --purge -y $PKG_NAME
test ! -x /usr/bin/sslocal
test ! -x /usr/bin/ssmanager
test ! -x /usr/bin/ssserver
test ! -x /usr/bin/ssservice
test ! -x /usr/bin/ssurl

# TEST: upgrade .deb package
apt-get install -y ./test_data/${PKG_NAME}_1.15.4-1-xdeb~${CODENAME}_amd64.deb
/usr/bin/sslocal --version   | grep "^shadowsocks 1.15.4$"
/usr/bin/ssmanager --version | grep "^shadowsocks 1.15.4$"
/usr/bin/ssserver --version  | grep "^shadowsocks 1.15.4$"
/usr/bin/ssservice --version | grep "^shadowsocks 1.15.4$"
/usr/bin/ssurl --version     | grep "^ssurl 1.15.4$"
apt-get install -y ./dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/sslocal --version   | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssmanager --version | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssserver --version  | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssservice --version | grep "^shadowsocks $PKG_VERSION$"
/usr/bin/ssurl --version     | grep "^ssurl $PKG_VERSION$"


exit 0
