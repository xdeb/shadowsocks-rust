FROM debian:bookworm AS busybox
ENV DEBIAN_FRONTEND=noninteractive
RUN set -eux ;\
    apt-get update ;\
    apt-get install --yes busybox

FROM debian:bookworm
ADD ./dist/x86_64/bookworm/sslocal \
    ./dist/x86_64/bookworm/ssmanager \
    ./dist/x86_64/bookworm/ssserver \
    ./dist/x86_64/bookworm/ssservice \
    ./dist/x86_64/bookworm/ssurl \
    ./dist/gnu/amd64/v2ray-plugin \
    /usr/bin/
COPY --from=busybox /bin/busybox /bin/busybox
RUN set -eux ;\
    ln -sv /bin/busybox /bin/ping ;\
    ln -sv /bin/busybox /sbin/ifconfig ;\
    ln -sv /bin/busybox /sbin/ip ;\
    ln -sv /bin/busybox /sbin/ipaddr ;\
    ln -sv /bin/busybox /sbin/route ;\
    ln -sv /bin/busybox /usr/bin/traceroute ;\
    ln -sv /bin/busybox /usr/bin/vi ;\
    ln -sv /bin/busybox /usr/bin/wget

# vim: ts=4 sw=4 et ft=dockerfile
