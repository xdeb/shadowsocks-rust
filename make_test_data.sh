#!/bin/bash

. ./make_test_data.env
PS4='\[\033[1;92m\]$(date +%FT%T%z) ${HOSTNAME}:${0}:${LINENO}\n+ \e[m\]'

set -eux

srcdir=$CI_PROJECT_DIR/$PKG_NAME-$PKG_VERSION
pwd=$(pwd)
vol_registry_cache=cargo-registry
tarball_name=$PKG_NAME-$PKG_VERSION.tar.gz
patched_tarball_name=$PKG_NAME-$PKG_VERSION-patched.tar.gz


###############################
##                           ##
##  Caching helper commands  ##
##                           ##
###############################

##  Caching `sccache` command
test -e /tmp/sccache ||
    wget -O- https://xdeb.gitlab.io/sccache/sccache-static_amd64.tar.gz | tar -C /tmp -zx

##  Caching `cargo-cache` command
test -e /tmp/cargo-cache ||
    wget -O- https://xdeb.gitlab.io/cargo-cache/cargo-cache-static_amd64.tar.gz | tar -C /tmp -zx

##  Caching `cargo-deb` command
test -e /tmp/cargo-deb ||
    wget -O- https://xdeb.gitlab.io/cargo-deb/cargo-deb-static_amd64.tar.gz | tar -C /tmp -zx


#################################
##                             ##
##  CI JOB: cache-source-code  ##
##                             ##
#################################

function prepare_source_code_in_container() {
    ctname=$PKG_NAME-$PKG_VERSION-builder-source

    # Local cached source code tarball file can speed up process
    test -e /tmp/$tarball_name || wget -O /tmp/$tarball_name https://github.com/shadowsocks/$PKG_NAME/archive/refs/tags/v$PKG_VERSION.tar.gz

    # Set up container
    docker rm -f $ctname
    docker run --name=$ctname -d -t \
        -v /tmp/$tarball_name:$CI_PROJECT_DIR/$tarball_name:ro \
        -v $pwd/src/Cargo.toml.patch:$CI_PROJECT_DIR/src/Cargo.toml.patch:ro \
        -w $CI_PROJECT_DIR \
        -h $ctname \
        --env-file ./make_test_data.env \
        alpine:latest

    docker exec -it $ctname sed -i s/https:/http:/ /etc/apk/repositories
    docker exec -it $ctname apk --no-cache add patch

    # Do patches
    docker exec -it $ctname tar -xf $tarball_name
    docker exec -itw $srcdir $ctname sh -ceux 'for f in ../src/*.patch; do patch -i $f; done'

    # Gain artifacts
    docker exec -it $ctname tar -C $CI_PROJECT_DIR -czf $patched_tarball_name $PKG_NAME-$PKG_VERSION
    docker cp $ctname:$CI_PROJECT_DIR/$patched_tarball_name /tmp/$patched_tarball_name
}

##  CI JOB: cache-source-code
prepare_source_code_in_container


##############
##          ##
##  BUILDS  ##
##          ##
##############

function build_in_container() {
    local os arch ctname vol_cache args

    os=$1
    arch=$2
    test -n "$os"
    test -n "$arch"

    ctname="$PKG_NAME-$PKG_VERSION-builder-$os-$arch"
    vol_cache="cache-$PKG_NAME-$PKG_VERSION-$os-$arch"
    args=""

    set +u
    test -n "$CARGO_REGISTRY_SOURCE" && args="$args -e CARGO_REGISTRY_SOURCE=$CARGO_REGISTRY_SOURCE"
    test -n "$RUSTUP_DIST_SERVER" && args="$args -e RUSTUP_DIST_SERVER=$RUSTUP_DIST_SERVER"
    test -n "$RUSTUP_UPDATE_ROOT" && args="$args -e RUSTUP_UPDATE_ROOT=$RUSTUP_UPDATE_ROOT"
    set -u

    test -f /tmp/$patched_tarball_name
    test -x /tmp/cargo-deb
    test -x /tmp/cargo-cache
    test -x /tmp/sccache

    # Set up builder container
    docker rm -f $ctname
    docker run --name=$ctname -d -t \
        --env-file ./make_test_data.env \
        -e TARGET_ARCH=$arch \
        -e RUSTC_WRAPPER=/usr/local/bin/sccache \
        -e SCCACHE_DIR=$CI_PROJECT_DIR/.cache/sccache-$PKG_NAME-$PKG_VERSION-$os-$arch \
        -e DEBIAN_FRONTEND=noninteractive \
        -v $vol_registry_cache:/usr/local/cargo/registry \
        -v $vol_cache:$CI_PROJECT_DIR/.cache \
        -v /tmp/$patched_tarball_name:$CI_PROJECT_DIR/$patched_tarball_name:ro \
        -v /tmp/cargo-deb:/usr/local/bin/cargo-deb:ro \
        -v /tmp/cargo-cache:/usr/local/bin/cargo-cache:ro \
        -v /tmp/sccache:/usr/local/bin/sccache:ro \
        -w $CI_PROJECT_DIR \
        -h $ctname \
        $args \
        rust:$RUST_VER-$os
    docker exec -it $ctname tar -xf $patched_tarball_name

    # Simulate `git clone`: Copy all source files from the repository into the container.
    sh -c "for f in *; do docker cp \$f $ctname:$CI_PROJECT_DIR/\$f; done"

    if [ "$os" = "alpine" ]; then
        docker exec -it $ctname sed -i s/https:/http:/ /etc/apk/repositories
        docker exec -it $ctname apk --no-cache add bash build-base

        # Do build
        docker exec -it $ctname ./ci-build-alpine.sh

        # Gain artifacts
        docker exec -it $ctname bash -c "tar -czf artifacts.tar.gz dist/$arch/$os/{sslocal,ssmanager,ssserver,ssservice,ssurl}"
        docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz
    else
        # Use local comstomized apt.conf to speed up `apt-get install`, if posible
        test -e /etc/apt/apt.conf && docker cp /etc/apt/apt.conf $ctname:/etc/apt/apt.conf

        # Do build
        docker exec -it $ctname ./ci-build.sh

        # Gain artifacts
        docker exec -it $ctname bash -c "tar -czf artifacts.tar.gz dist/$arch/$os/{sslocal,ssmanager,ssserver,ssservice,ssurl} dist/*.deb"
        docker cp $ctname:$CI_PROJECT_DIR/artifacts.tar.gz /tmp/${ctname}_artifacts.tar.gz
    fi
}

##  CI JOB: build-bullseye-amd64
build_in_container bullseye x86_64

##  CI JOB: build-bookworm-amd64
build_in_container bookworm x86_64

##  CI JOB: build-alpine-amd64
build_in_container alpine x86_64

##  CI JOB: build-alpine-i386
build_in_container alpine i686


#############
##         ##
##  TESTS  ##
##         ##
#############

test -x $pwd/ci-test.sh
test -x $pwd/ci-test-alpine.sh
test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-bullseye-x86_64_artifacts.tar.gz
test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-bookworm-x86_64_artifacts.tar.gz
test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine-x86_64_artifacts.tar.gz
test -f /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine-i686_artifacts.tar.gz
test -d $pwd/test_data

args=""
test -e /etc/apt/apt.conf && args="-v /etc/apt/apt.conf:/etc/apt/apt.conf:ro"

for codename in bullseye bookworm focal jammy noble
do
    case $codename in
        bullseye|bookworm) os=debian ;;
        focal|jammy|noble) os=ubuntu ;;
    esac

    test -f $pwd/test_data/${PKG_NAME}_1.15.4-1-xdeb~${codename}_amd64.deb

    docker run --rm -it \
        -v $pwd/ci-test.sh:/bin/ci-test.sh:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-bullseye-x86_64_artifacts.tar.gz:/tmp/bullseye_artifacts.tar.gz:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-bookworm-x86_64_artifacts.tar.gz:/tmp/bookworm_artifacts.tar.gz:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine-x86_64_artifacts.tar.gz:/tmp/alpine_artifacts.tar.gz:ro \
        -v /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine-i686_artifacts.tar.gz:/tmp/alpine_artifacts2.tar.gz:ro \
        -v $pwd/test_data/${PKG_NAME}_1.15.4-1-xdeb~${codename}_amd64.deb:/test_data/${PKG_NAME}_1.15.4-1-xdeb~${codename}_amd64.deb:ro \
        -h test-$codename \
        --env-file ./make_test_data.env \
        -e CODENAME=$codename \
        -e DEBIAN_FRONTEND=noninteractive \
        $args \
        $os:$codename sh -c "set -eux
        tar -xf /tmp/bullseye_artifacts.tar.gz
        tar -xf /tmp/bookworm_artifacts.tar.gz
        tar -xf /tmp/alpine_artifacts.tar.gz
        tar -xf /tmp/alpine_artifacts2.tar.gz
        ls -lAF --color /dist/
        ci-test.sh"
done

docker run --rm -it \
    -v $pwd/ci-test-alpine.sh:/bin/ci-test-alpine.sh:ro \
    -v /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine-x86_64_artifacts.tar.gz:/tmp/alpine_artifacts.tar.gz:ro \
    -v /tmp/$PKG_NAME-$PKG_VERSION-builder-alpine-i686_artifacts.tar.gz:/tmp/alpine_artifacts2.tar.gz:ro \
    -h test-alpine \
    --env-file ./make_test_data.env \
    alpine:latest sh -c "set -eux
    tar -xf /tmp/alpine_artifacts.tar.gz
    tar -xf /tmp/alpine_artifacts2.tar.gz
    ls -lAF --color /dist/
    sed -i s/https:/http:/ /etc/apk/repositories
    apk --no-cache add bash
    ci-test-alpine.sh"


###############################
#                            ##
#  COPY OUT .DEB, .APK FILE  ##
#                            ##
###############################

mkdir -p test_data
rm -fv test_data/*.deb test_data/*.apk

tmpdir=$(mktemp -d)
tar -C $tmpdir -xf /tmp/$PKG_NAME-$PKG_VERSION-builder-bullseye-x86_64_artifacts.tar.gz
tar -C $tmpdir -xf /tmp/$PKG_NAME-$PKG_VERSION-builder-bookworm-x86_64_artifacts.tar.gz
cp -v $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bullseye_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~focal_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~bookworm_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~jammy_amd64.deb \
    $tmpdir/dist/${PKG_NAME}_${PKG_VERSION}-${PKG_RELEASE}-xdeb~noble_amd64.deb \
    test_data/
rm -rf $tmpdir


exit 0
